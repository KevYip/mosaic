"use strict";

//The logic is as follows:

/*
	1. User loads a file from their machine.
	2. File gets drawn to a canvas element.
	3. Iterate row by row, by keeping track of the y position in the image. Increment y by TILE_HEIGHT until y > image height.
	Repeat the below for each row:
		a. For each row, iterate through each tile in that row by incrementing x by TILE_WIDTH until x > image width. 
			i. Get the rgb of all the pixels in the current 'tile' in the canvas (between (x,y) to (x+TILE_WIDTH, y+TILE_HEIGHT))
		b. Once we've gotten all the colours of a row, we pass the colours to the Mosaic's drawMosaicRow function which retrieves
		the tile of each colour and draws a row of tiles onto the screen
	4. Ready to serve
*/

var Main = {
	data: {
		ctx: {},
		file: {},
		blankColor: 'ffffff',
	},

	init: function() {
		Canvas.init();
		Mosaic.init();

		this.data.ctx = Canvas.getCtx();
		this.data.file = document.getElementById('file');

		document.getElementById('closeError').addEventListener("click", function() {
			document.getElementById('errorWindow').className = "hide";
		});

		this.setupFileListener();
		
	},

	setupFileListener: function() {
		var self = this;

		//When the user selects an image from their computer, we read the file, then render it to the canvas
		this.data.file.addEventListener('change', function(e) { 
			//Read the selected file that the user selected and draw it to the canvas
			//http://stackoverflow.com/questions/10906734/how-to-upload-image-into-html5-canvas
			var file = e.target.files[0];

			if(!file) {
				return;
			}

			document.getElementById('filename').innerHTML = file.name;

			document.getElementById('uploadButton').style.display = "none";

			Canvas.reset();

			//Create a new file reader which will read the selected file
			var reader = new FileReader();

			//The file reader has loaded the file
			reader.onload = function(event){
		        var img = new Image();

		        //Set the image source to the selected file
		        img.src = event.target.result;

		        //Image has loaded
		        img.onload = function(){
					//Call the Canvas' function with the selected image to draw the original image to the canvas
					Canvas.drawOriginalImage(img);

					//Fade the image out while we draw the mosaic
					Canvas.fade();

					//When the image has finished drawing, we run the main program
					//i.e. start doing calculations of average colours for the tiles in the mosaic and drawing them
		            self.run();
		        }
		    }

			reader.readAsDataURL(file);

		});
	},

	run: function() {
		var self = this;
		
		//Set up the mosaic for rendering
		Mosaic.clearMosaic();
		Mosaic.setWidth(Canvas.getWidth());
		Mosaic.setHeight(Canvas.getHeight());

		//This function processes a row of the image by calculating
		//colors and then calling the Mosaic component's
		//draw row function. We only want to draw
		//the next row after we've finished drawing the current row
		var processRow = function(y) {

			//Calculate the average colors for all the tiles in the row
			var row = self.calculateRowTileColors(y);

			//Draw the mosaic row
			var p = Mosaic.drawMosaicRow(row);

			//When finished drawing, we process the next row
			p.then(function() {
				y += TILE_HEIGHT; //Make the y coordinate go to the next row by adding TILE_HEIGHT
				if(y < Canvas.getHeight()) { //Have we reached the last row yet?
					processRow(y);
				} else { //Done!
					document.getElementById('uploadButton').style.display = "";
				}
			}).catch(function(error) {
				document.getElementById('errorWindow').className = "show";
				document.getElementById('uploadButton').style.display = "";

				self.data.file.value = "";
				console.log(error);
			});
		};

		processRow(0);
	},

	calculateRowTileColors: function(y) {
		var row = [];

		//Iterate through the row and calculate the average color for each tile in the row.
		//x coordinate increments by TILE_WIDTH to go to the next tile in the row
		for(var x = 0; x < Canvas.getWidth(); x += TILE_WIDTH) {
			row.push(this.calculateAverageTileColor(x, y));
		}
		
		return row;
	},

	//Calculates the average color of all pixels in specified width and height (tile) and returns
	//the value in hexadecimal format
	calculateAverageTileColor: function(x, y) {

		//Get the pixel data (r,g,b,a) of all the pixels in a tile of specified width and height
		//The format returned is an array of the format [r1,g1,b1,a1,r2,g2,b2,a2, ...] where r1
		//is the red value of pixel 1 and so on.
		var imageData = this.data.ctx.getImageData(x,y,TILE_WIDTH,TILE_HEIGHT);
		var pixelData = imageData.data;

		var redSum = 0, greenSum = 0, blueSum = 0, pixelCount = 0;

		//Loop through each pixel in the tile area
		for(var i = 0; i < pixelData.length; i += 4) {
			//Opacity of the pixel
			var a = pixelData[i+3];

			//Add visible (opacity > 0) pixel colors to their repective sums
			//for later calculation of the average colors for this tile
			if(a > 0) {
				redSum += pixelData[i];
				greenSum += pixelData[i+1];
				blueSum += pixelData[i+2];
				pixelCount++;
			}
		}

		//No visible pixels, return the default blank color e.g. white
		if(pixelCount == 0) {
			return this.data.blankColor;
		}

		var avgRed = Math.round(redSum / pixelCount);
		var avgGreen = Math.round(greenSum / pixelCount);
		var avgBlue = Math.round(blueSum / pixelCount);

		return this.rgbToHex(avgRed, avgGreen, avgBlue);

	},

	rgbToHex: function(r, g, b) {
		return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},

}

window.onload = function() {
	Main.init();
}

