"use strict";

//This handles the retrieval of colors on the server and the rendering of the mosaic
var Mosaic = {
	data: {
		mosaic: {}
	},

	init: function() {
		this.data.mosaic = document.getElementById('mosaic');
	},

	//Loads and draws a row of tiles given the tile colors in "colors". 
	//Loads each tile of the row asynchronously instead of sequentially
	//Returns a promise which resolves when we've drawn the whole row
	drawMosaicRow: function(colors) {
		var self = this;

		var promise = new Promise(function(resolve, reject) {

			//For each tile, push loadMosaicTile function's promise with the the tile's color into an array...
			var promises = [];
			colors.forEach(function(color,index) {
				promises.push(self.loadMosaicTile(color));
			});

			var tilesSvg = "<div>";

			//...then execute all of them at the same time
			//and when all the promises resolve (i.e. all tiles are loaded), 
			//we draw the retrieved tiles in order
			Promise.all(promises).then(function(tiles) {
				tiles.forEach(function(tile, index) {
					tilesSvg += tile;
				});
				tilesSvg += "</div>";
				self.data.mosaic.insertAdjacentHTML('beforeend', tilesSvg);
				resolve();
			}).catch(function(error) {
				reject(error);
			})
		});

		return promise;

	},
	//Makes a request to the server for a color. 
	//Returns a promise that will be resolved with the tile response from the server when it has loaded
	loadMosaicTile: function(colorHex) {
		var self = this;

		var promise = new Promise(function(resolve, reject) {
			//Make an asynchronous request to the server for a color
			var request = new XMLHttpRequest();
			request.addEventListener("load", function() {
				if(request.status == 200) {
					//Once the tile has loaded, resolve the promise with the tile
					var tile = this.responseText;
					resolve(tile);
				} else {
					reject(Error(request.statusText))
				}
			});
			request.addEventListener("error", function() {
				reject(Error("Couldn't retrieve tile from server."));
			});

			request.open("GET", "/color/" + colorHex);
			request.send();
		});

		return promise;
	},
	setWidth: function(width) {
		this.data.mosaic.style.width = width + "px";
	},
	setHeight: function(height) {
		this.data.mosaic.style.height = height + "px";
	},
	clearMosaic: function() {
		this.data.mosaic.innerHTML = '';
	},
	getMosaic: function() {
		return this.data.mosaic;
	}
}