"use strict";

//This handles all the functionality required to retrieve the original image that the user selects
//and puts it in a canvas for later processing of the tile colours
var Canvas = {
	data: {
		canvas: {},
		ctx: {}
	},

	init: function() {
		this.data.canvas = document.getElementById('canvas');
		this.data.ctx = this.data.canvas.getContext('2d');
	},

	//Draw an image to the canvas.
	drawOriginalImage: function(img) {
        this.data.canvas.width = img.width;
        this.data.canvas.height = img.height;
        this.data.ctx.drawImage(img,0,0);

	},
	fade: function() {
		var self = this;

        this.data.canvas.className = "";
        setTimeout(function() {
        	self.data.canvas.className = "fade";
        }, 100)
	},
	reset: function() {
		this.data.canvas.innerHTML = "";
        this.data.canvas.className = "";
	},
	getCtx: function() {
		return this.data.ctx;
	},
	getWidth: function() {
		return this.data.canvas.width;
	},
	getHeight: function() {
		return this.data.canvas.height;
	}
};